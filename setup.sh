#!/bin/bash

# File where the URL will be stored
CONF_FILE="$HOME/signage.conf"
# Openbox autostart configuration file
AUTOSTART_FILE="$HOME/.config/openbox/autostart"

# Set the timezone to Europe/Brussels
sudo timedatectl set-timezone "Europe/Brussels"

# Read configuration file if it exists
if [ -f "$CONF_FILE" ]; then
    # Load configuration settings
    . "$CONF_FILE"
else
    # Copy the default configuration file to the user's home directory if it does not exist
    cp signage.conf "$CONF_FILE"

    # If the copy command fails, then exit
    if [ ! -f "$CONF_FILE" ]; then
        echo "Configuration file not found and default could not be copied. Exiting."
        exit 1
    fi

    # Load the newly copied default configuration settings
    . "$CONF_FILE"
fi

# Prompt for URL if not set
if [ -z "$URL" ]; then
    read -p "Enter the URL to display in kiosk mode: " URL
    echo "URL=$URL" >> "$CONF_FILE"
fi

# Update and Upgrade
#sudo apt update && sudo apt upgrade -y
sudo apt update

# Install necessary packages
sudo apt install --no-install-recommends xserver-xorg x11-xserver-utils xinit openbox chromium-browser unclutter -y

# Enable SSH
sudo systemctl enable ssh
sudo systemctl start ssh

# Set up auto-login
sudo raspi-config nonint do_boot_behaviour B2

# Create autostart directory
mkdir -p "$(dirname "$AUTOSTART_FILE")"

# Schedule reboots at 8 AM and 12 PM
(crontab -l 2>/dev/null; echo "0 8 * * * /sbin/reboot") | crontab -
(crontab -l 2>/dev/null; echo "0 12 * * * /sbin/reboot") | crontab -

# Write autostart configuration
cat > "$AUTOSTART_FILE" <<EOF
xset s noblank
xset s off
xset -dpms
unclutter -idle $UNCLUTTER_TIME -root &
chromium-browser $KIOSK_MODE "$URL" --noerrdialogs --disable-infobars --incognito &
EOF

# Write .xinitrc configuration
cat > "$HOME/.xinitrc" <<EOF
exec openbox-session
EOF

# Configure .bash_profile for auto startx
echo "[[ -z \$DISPLAY && \$XDG_VTNR -eq 1 ]] && exec startx" >> ~/.bash_profile

echo "Autostart configuration is set up with the following options:"
echo "URL: $URL"
echo "Kiosk Mode: $KIOSK_MODE"
echo "Unclutter Time: $UNCLUTTER_TIME"

# Final Message
echo "Setup complete. Please reboot the Raspberry Pi."
